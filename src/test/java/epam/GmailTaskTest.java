package epam;

import org.apache.logging.log4j.LogManager;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;


public class GmailTaskTest {

    private static final Logger LOG = LogManager.getLogger(GmailTaskTest.class);
    private static final String HOME_PAGE = "https://mail.google.com/";
    private static final String PATH_TO_DRIVER = "src/main/resources/chromedriver.exe";
    private static final String WEB_DRIVER_NAME = "webdriver.chrome.driver";
    private static final String EMAIL_LOGIN = "ihor.chushchak.kn.2017@lpnu.ua";
    private static final String EMAIL_PASSWORD = "14.06.2000";
    private static final String EMAIL_Text_TO_SEND = "Go na coffee!";
    private static WebDriver driver;
    private static WebElement element;
    private static WebDriverWait wait;

    static void clickButtonByXpath(String reference) {
        driver.findElement(By.xpath(reference)).click();
    }

    static void clickButtonByCssSelector(String reference) {
        driver.findElement(By.cssSelector(reference)).click();
    }


    @BeforeClass
    static void initializeObjects() {
        System.setProperty(WEB_DRIVER_NAME, PATH_TO_DRIVER);
        driver = new ChromeDriver();
        driver.manage().timeouts()
                .implicitlyWait(30, TimeUnit.SECONDS);
        driver.get(HOME_PAGE);
    }

    @Test
    void testPageTitle() {
        LOG.info(driver.getTitle());
        assertNotNull(driver.getTitle());
    }

    @Test
    void initializeLoginGmailTest() {
        element = driver.findElement(By.cssSelector("#identifierId"));
        element.sendKeys(EMAIL_LOGIN);
        clickButtonByXpath("//*[@id=\"identifierNext\"]/span");
        element = driver.findElement(By.xpath("//*[@id=\"password\"]/div[1]/div/div[1]/input"));
        assertEquals("Увійти", driver.findElement(By.xpath("//*[@id=\"headingText\"]/span")).getText());
    }

    @Test(dependsOnMethods = {"initializeLoginGmailTest"})
    void initializePasswordGmailTest() {
        element = driver.findElement(By.xpath("//*[@id=\"password\"]/div[1]/div/div[1]/input"));
        element.sendKeys(EMAIL_PASSWORD);
        wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(By.id("passwordNext")));
        clickButtonByXpath("//*[@id='passwordNext']");
        assertEquals(EMAIL_LOGIN, driver.findElement(By.cssSelector("#profileIdentifier")).getText());

    }

    @Test(dependsOnMethods = {"initializePasswordGmailTest"})
    void sendEmailTest() {
        clickButtonByCssSelector("#\\:iv > div > div");
        driver.findElement(By.xpath("//*[@id=\":of\"]")).sendKeys(EMAIL_LOGIN);
        driver.findElement(By.xpath("//*[@id=\":nx\"]")).sendKeys(EMAIL_Text_TO_SEND);
        clickButtonByCssSelector("#\\:nn");
    }

    @Test(dependsOnMethods = {"sendEmailTest"})
    void checkWhetherEmailIsSendTest() {
        clickButtonByXpath("//*[@id='link_vsm']");
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm");
        LocalDateTime now = LocalDateTime.now();
        assertEquals(dtf.format(now) + " (0 хвилин тому)", driver.findElement(By.xpath("/html/body/div[7]/div[3]/div/div[2]" +
                "/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div[3]/div/table/tr/td[1]/div[2]/div[2]/div" +
                "/div[3]/div/div/div/div/div/div[1]/div[2]/div[1]/table/tbody/tr[1]/td[2]/div/span[2]")).getText());
    }

    @AfterClass
    static void closeResources() {
        driver.quit();
    }
}
